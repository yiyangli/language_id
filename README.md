# Language Identification

## Description
Language Identification (LID) systems are used to classify the spoken language from a given audio sample and are the first step for Automatic Speech Recognition (ASR) systems.

## Requirements
This project was implemented in Python and Tensorflow. We recommend using python and a virtual env.

```
virtualenv -p python .env
source .env/bin/activate
pip install -r requirements.txt
```

When you're done working on the project, deactivate the virtual environment with `deactivate`.

## To build dataset
Split train/dev/test set
```
cd ~/data/
python build_src.py --root ~/language-id/ --ext youtube --ratio 0.9
```

Convert .wav files to images
```
cd ~/data/
python submit.py
```

Balance classes
```
cd ~/imgs/
run balance_cls.ipynb
```

## To create an experiment
We created a `base_model` directory for you under the `experiments` directory. It countains a file `params.json` which sets the parameters for the experiment. It looks like
```json
{
    "learning_rate": 1e-3,
    "batch_size": 32,
    "num_epochs": 10,
    ...
}
```
For every new experiment, you will need to create a new directory under `experiments` with a similar `params.json` file.

## To train
```
cd ~/tensorflow/
python train.py --data_dir $DATA_DIR --model_dir $MODEL_DIR
e.g python train.py --data_dir ../imgs/ --model_dir experiments/base_model/

# train from last weights
python train.py --data_dir $DATA_DIR --model_dir $MODEL_DIR --restore_from $MODEL_DIR/last_weights
```

## To evaluate
```
cd ~/tensorflow/
python evaluate.py --data_dir $DATA_DIR --model_dir $MODEL_DIR --restore_from $MODEL_DIR/best_weights
```