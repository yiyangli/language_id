import os
import argparse
from random import shuffle

parser = argparse.ArgumentParser()
parser.add_argument('--root', default=None, help="root")
parser.add_argument('--ext', default=None, help="number of bins for computing mfcc")
parser.add_argument('--ratio', default=None, help="number of frames to get when computing mfcc")
parser.add_argument('--lang', default=None, help="the language to process")

args = parser.parse_args()
ROOT = args.root
ext = args.ext
ratio = float(args.ratio)

def mkdir(dst_dir):
    """Make a directory `dst_dir` if it hasn't been created, otherwise give a warning"""
    if not os.path.exists(dst_dir):
            os.mkdir(dst_dir)
    else:
        print("Warning: dir {} already exists".format(dst_dir))
        
def writedata(lang,step,extend,lang_name,status):
    i = 0
    c = 0
    mkdir(ROOT+"src/"+extend+"_"+status)
    while i < len(lang):
        if i != 0 and (i+1)%step == 0:
            f = open(ROOT+"src/"+extend+"_"+status+"/"+lang_name+"_"+str(c)+"_"+extend+".txt","w")
            for j in lang[(i+1)-step:(i+1)]:
                f.write(j+"\n")
            f.close()
            c += 1
        i += 1

        if i == len(lang) and i%step != 0:
            f = open(ROOT+"src/"+extend+"_"+status+"/"+lang_name+"_"+str(c)+"_"+extend+".txt","w")
            for j in lang[-(i%step):]:
                f.write(j+"\n")
            f.close()
            
def addwav(lang, l):
    for i in os.listdir(lang):
        if i[-4:] == ".wav":
            l.append(lang+i)
            

def sanitycheck(extend,status,eng,ger,fre,spa,cn):
    c = 0
    root = ROOT+"src/"+extend+"_"+status+"/"
    eng_check = []
    ger_check = []
    fre_check = []
    spa_check = []
    cn_check = []
    for i in sorted(os.listdir(ROOT+"src/"+extend+"_"+status+"/")):
        if i[:3] == "eng":
            with open(root+i,"r") as f:
                for row in f:
                    eng_check.append(row.strip())
        elif i[:3] == "ger":
            with open(root+i,"r") as f:
                for row in f:
                    ger_check.append(row.strip())
        elif i[:3] == "fre":
#             print i
            with open(root+i,"r") as f:
                for row in f:
                    fre_check.append(row.strip())
                    c += 1
#                 print c
        elif i[:3] == "spa":
            with open(root+i,"r") as f:
                for row in f:
                    spa_check.append(row.strip())
        elif i[:3] == "chi":
            with open(root+i,"r") as f:
                for row in f:
                    cn_check.append(row.strip())




    assert len(eng_check) == len(set(eng_check))
    assert len(eng) == len(set(eng))
    assert set(eng_check) == set(eng)

    assert len(ger_check) == len(set(ger_check))
    assert len(ger) == len(set(ger))
    assert set(ger_check) == set(ger)

    assert len(fre_check) == len(set(fre_check))
    assert len(fre) == len(set(fre))
    assert set(fre_check) == set(fre)

    assert len(spa_check) == len(set(spa_check))
    assert len(spa) == len(set(spa))
    assert set(spa_check) == set(spa)


    assert len(cn_check) == len(set(cn_check))
    assert len(cn) == len(set(cn))
    assert set(cn_check) == set(cn)

if __name__ == "__main__":
    root = ROOT+"data/"
    # english
    eng_you = root+ext+"/english/"

    # german
    ger_you = root+ext+"/german/"

    # french
    fre_you = root+ext+"/french/"

    # spanish
    spa_you = root+ext+"/spanish/"

    # chinese
    cn_you = root+ext+"/chinese/"


    eng_you_list = []
    ger_you_list = []
    fre_you_list = []
    spa_you_list = []
    cn_you_list = []

    addwav(eng_you,eng_you_list)
    addwav(ger_you,ger_you_list)
    addwav(fre_you,fre_you_list)
    addwav(spa_you,spa_you_list)
    addwav(cn_you,cn_you_list)

    shuffle(eng_you_list)
    shuffle(ger_you_list)
    shuffle(fre_you_list)
    shuffle(spa_you_list)
    shuffle(cn_you_list)

    step = 2
    
    eng_list = eng_you_list
    ger_list = ger_you_list
    fre_list = fre_you_list
    spa_list = spa_you_list
    cn_list = cn_you_list

    cut0_e = int(len(eng_list)*ratio)
    cut1_e = int(len(eng_list)*ratio+len(eng_list)*(1-ratio)/2)
        
    writedata(eng_list[:cut0_e],step,ext,"english","train")
    writedata(eng_list[cut0_e:cut1_e],step,ext,"english","dev")
    writedata(eng_list[cut1_e:],step,ext,"english","test")

    cut0_g = int(len(ger_list)*ratio)
    cut1_g = int(len(ger_list)*ratio+len(ger_list)*(1-ratio)/2)
    writedata(ger_list[:cut0_g],step,ext,"german","train")
    writedata(ger_list[cut0_g:cut1_g],step,ext,"german","dev")
    writedata(ger_list[cut1_g:],step,ext,"german","test")

    cut0_f = int(len(fre_list)*ratio)
    cut1_f = int(len(fre_list)*ratio+len(fre_list)*(1-ratio)/2)
    writedata(fre_list[:cut0_f],step,ext,"french","train")
    writedata(fre_list[cut0_f:cut1_f],step,ext,"french","dev")
    writedata(fre_list[cut1_f:],step,ext,"french","test")

    cut0_s = int(len(spa_list)*ratio)
    cut1_s = int(len(spa_list)*ratio+len(spa_list)*(1-ratio)/2)
    writedata(spa_list[:cut0_s],step,ext,"spanish","train")
    writedata(spa_list[cut0_s:cut1_s],step,ext,"spanish","dev")
    writedata(spa_list[cut1_s:],step,ext,"spanish","test")

    cut0_c = int(len(cn_list)*ratio)
    cut1_c = int(len(cn_list)*ratio+len(cn_list)*(1-ratio)/2)
    writedata(cn_list[:cut0_c],step,ext,"chinese","train")
    writedata(cn_list[cut0_c:cut1_c],step,ext,"chinese","dev")
    writedata(cn_list[cut1_c:],step,ext,"chinese","test")

    sanitycheck(ext,"train",eng_list[:cut0_e],ger_list[:cut0_g],fre_list[:cut0_f],spa_list[:cut0_s],cn_list[:cut0_c])
    sanitycheck(ext,"dev",eng_list[cut0_e:cut1_e],ger_list[cut0_g:cut1_g],fre_list[cut0_f:cut1_f],spa_list[cut0_s:cut1_s],cn_list[cut0_c:cut1_c])
    sanitycheck(ext,"test",eng_list[cut1_e:],ger_list[cut1_g:],fre_list[cut1_f:],spa_list[cut1_s:],cn_list[cut1_c:])

