from subprocess import check_call

if __name__ == "__main__":
    langs = ["english", "german", "french", "spanish", "chinese"]
    
    # vox
#     extend = "voxforge"
#     nums_train = [78, 22, 21, 22, 0]
#     nums_dev = [5, 2, 2, 2, 0]
#     nums_test = [5, 2, 2, 2, 0]
    
    # eu
#     extend = "eu_repo"
#     nums_train = [29, 20, 26, 16, 0] 
#     nums_dev = [2, 2, 2, 1, 0] 
#     nums_test = [2, 2, 2, 1, 0]
    
    # you
    extend = "youtube"
    nums_train = [783, 553, 571, 1922, 102]
    nums_dev = [43, 31, 32, 107, 6]
    nums_test = [4, 31, 32, 108, 6]
    
    nums_train = [1, 1, 1, 1, 1]
    nums_dev = [1, 1, 1, 1, 1]
    nums_test = [1, 1, 1, 1, 1]
    
#     # mix
#     extend = "mix"
#     nums_train = [0, 0, 0, 0, 150]
#     nums_dev = [0, 0, 0, 0, 9]
#     nums_test = [0, 0, 0, 0, 9]
    
    
    nums = []
    for i in range(5):
        for j in ["train","dev","test"]:
            cmd_py = "source /f/gfs1/yiyangli/.lang_env/bin/activate"
            cmd_py += ";export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/dist-packages"
            cmd_py += ";cd /f/gfs1/yiyangli/language-id/data/"
            cmd_py += ";python wav_to_img.py --submit True --dir {DIR} --num_bins 40 --num_frames 1498 --lang {lang}".format(DIR=extend+"_"+j,lang=langs[i])
            print "cmd_py: ", cmd_py
            
            if j == "train":
                nums = nums_train
            elif j == "dev":
                nums = nums_dev
            else:
                nums = nums_test
            
            if nums[i] == 1:
                n=""
            else:
                n="-"+str(nums[i])
            
            cmd = "qsub -l h=\"grid-cpu16-1.az.aisense.com\" -cwd -shell no -b yes -N {ext}_{lang} -t 1{n} -q all.q -pe smp 1 bash -c \"{cmd}\"".format(ext=extend,lang=langs[i],n=n,cmd=cmd_py)
            check_call(cmd, shell=True)
            