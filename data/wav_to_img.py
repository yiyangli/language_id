""" Convert raw .wav files to images

This script follows the procedures below:

    1. Split raw .wav files that are longer than 1min into small audio chunks, and leave those less than 1min as they are, e.g audio of 20min25s -> 20 * 1min audio and 25s audio

    2. Convert Chunks to 16 kHz

    3. Split audios that are in sterotype and pick channel 1

    4. Compute MFCC of all audio chunks

    5. Convert MFCC features to images
"""

import numpy as np
import argparse
import os
import time
import sys
import collections
import scipy.misc
from scipy.io.wavfile import read as read_wav
import json
from subprocess import check_call
from scikits.audiolab import Format, Sndfile
import kaldi_io
import random
from random import shuffle
from pydub import AudioSegment
import wave
from shutil import copyfile


parser = argparse.ArgumentParser()
parser.add_argument('--submit', default=None, help="Boolean flag to specify if submit to SGE or not")
parser.add_argument('--dir', default=None, help="dir containing src files of one language")
parser.add_argument('--num_bins', default=None, help="number of bins for computing mfcc")
parser.add_argument('--num_frames', default=None, help="number of frames to get when computing mfcc")
parser.add_argument('--lang', default=None, help="the language to process")


# Load the parameters
args = parser.parse_args()
DIR = args.dir
NUM_BINS = int(args.num_bins)
NUM_FRAMES = int(args.num_frames) # 10s -> 998, 15s -> 1498
SUBMIT = args.submit
LANG = args.lang
SAMPLE_RATE = 16000


def mkdir(dst_dir):
    """Make a directory `dst_dir` if it hasn't been created, otherwise give a warning"""
    if not os.path.exists(dst_dir):
            os.mkdir(dst_dir)
    else:
        print("Warning: dir {} already exists".format(dst_dir))
        
def rmdir(dst_dir):
    """Remove a directory `dst_dir`"""
    if os.path.exists(dst_dir):
        cmd = "rm -rf {dst_dir}".format(dst_dir=dst_dir)
        check_call(cmd, shell=True)



def get_chunks(wav_files, chunk_dir):
    # ----get the audio chunk----
    """Generate audio chunks from each audio in `wav_dir`
    
    Args:
        wav_files: paths to each .wav file
        chunk_dir: directory to store audio chunks
    """
    
    print("start generating audio chunks")
    mkdir(chunk_dir)
    
    # go through wav files
    for audio in wav_files:
        
        # only interest in .wav files
        if audio[-4:] != ".wav":
            continue
        
        # READ FILE
        sound = AudioSegment.from_file(audio)
        
        # WRITE FILE
        # interest in audio files that are longer than 5000ms (5s)
        # split audio longer than 60000ms (1min) into chunks of 1min
        # e.g .wav file of 30s -> directly copy to chunk dir
        #     .wav file of 2min20s -> 2 * 1min audio chunk + 20s audio chunk 
        #     .wav file of 3s -> skip
        if len(sound) <= 60000 and len(sound) >= 5000:
            ind =audio.rfind("/")
            copyfile(audio,chunk_dir+audio[ind+1:])
        elif len(sound) > 60000:
            count = len(sound) / 60000
            for i in range(count):
                part = sound[i*60000:(i+1)*60000]
                ind =audio.rfind("/")
                part.export(chunk_dir+audio[ind+1:][:-4]+"_"+str(i)+".wav", format="wav")
            if count*60000 + 5000 < len(sound):
                part = sound[count*60000:]
                ind =audio.rfind("/")
                part.export(chunk_dir+audio[ind+1:][:-4]+"_"+str(count)+".wav", format="wav")
            
    print("finish generating audio chunks")

def convert(chunk_dir):
    """Convert audio chunks to 16kHz
    
    Args:
        chunk_dir: directory contains all audio chunks
    """
    
    fs = os.listdir(chunk_dir)
    for f in fs:
        if f[-4:] == ".wav":
            # convert the sample rate of audios to 16kHz
            cmd = "sox {inf} -r {sr} {out}".format(inf=chunk_dir+f,sr=SAMPLE_RATE,out=chunk_dir+"a"+f)
            check_call(cmd,shell=True)
            cmd = "rm -rf {src}".format(src=chunk_dir+f)
            check_call(cmd,shell=True)

def split(chunk_dir):
    """Split the channels of audios that are in sterotype and keep channel 1
        
    Args:
        chunk_dir: directory contains audio chunks
    """
    fs = os.listdir(chunk_dir)
    for f in fs:
        if f[-4:] != ".wav":
            continue

        # READ FILE
        wf = wave.open(chunk_dir+f, 'rb')
        
        # CHECK channel number
        if wf.getnchannels() == 1:
            continue
        elif wf.getnchannels() == 2:
            cmd = "sox {inf} {out} remix 1".format(inf=chunk_dir+f, out=chunk_dir+"b"+f)
            check_call(cmd,shell=True)
            cmd = "rm -rf {src}".format(src=chunk_dir+f)
            check_call(cmd,shell=True)
        else:
            cmd = "rm -rf {src}".format(src=chunk_dir+f)
            check_call(cmd,shell=True)
            
        
def compute_mfcc_cmvn(chunk_dir, mfcc_scp, mfcc_ark, cmvn_scp, cmvn_ark, cmvn_feat_ark):
    """Compute MFCC with CMVN of audio chunks
    
    Args:
        chunk_dir: directory contains audio chunks
        mfcc_scp: scp file for computing MFCC
        mfcc_ark: ark file for computing MFCC
        cmvn_scp: scp file for applying CMVN
        cmvn_ark: ark file for applying CMVN
        cmvn_feat_ark: ark file for computing MFCC with CMVN
    """
    
    # ----compute mfcc----
    print("start computing mfcc")
    f_write = open(mfcc_scp,"w")
    flag = False
    for audio in os.listdir(chunk_dir):
        if audio[-4:] == ".wav":
            f_write.write("{fileid} {chunk_dir}{filename}\n".format(fileid=audio[:-4], chunk_dir=chunk_dir, filename=audio))
            flag = True
    f_write.close()
    
    # if there's no audio chunk, just return 
    if not flag:
        return
    
    # Command for computing mfcc
    # Use the second command if you want to applying VTLN, we set VTLN warp factor to 0.9 for now. 
    # Ideally, you should train a model on audios to get the best VTLN warp factor
    cmd_mfcc = "compute-mfcc-feats --use-energy=false --num-mel-bins={num_bins} --num-ceps={num_ceps} scp:{scp_file} ark:{ark_file}".format(num_bins=NUM_BINS, num_ceps=NUM_BINS, scp_file=mfcc_scp, ark_file=mfcc_ark)

#     cmd_mfcc = "compute-mfcc-feats --vtln-warp=0.9 --use-energy=false --num-mel-bins={num_bins} --num-ceps={num_ceps} scp:{scp_file} ark:{ark_file}".format(num_bins=NUM_BINS, num_ceps=NUM_BINS, scp_file=mfcc_scp, ark_file=mfcc_ark)
    check_call(cmd_mfcc, shell=True)
    
    # Commands for applying CMVN after computing MFCC
    # you can comment these lines if you wish not to apply CMVN, just remeber to set cmvn_feat_ark = mfcc_ark
    cmd_cmvn = "compute-cmvn-stats ark:{mfcc_ark} ark,scp:{cmvn_ark},{cmvn_scp}".format(mfcc_ark=mfcc_ark,cmvn_ark=cmvn_ark,cmvn_scp=cmvn_scp)
    cmd_cmvn += ";apply-cmvn scp:{cmvn_scp} ark:{mfcc_ark} ark:{cmvn_feat_ark}".format(cmvn_scp=cmvn_scp,mfcc_ark=mfcc_ark,cmvn_feat_ark=cmvn_feat_ark)
    check_call(cmd_cmvn, shell=True)
    
    print("finish computing mfcc")


def get_img(img_dir, cmvn_feat_ark, label, id, extend):
    """Convert feaetures in ark files to images and assign labels in their filenames
    
    Args:
        img_dir: directory to store generated images
        cmvn_feat_ark: ark file contains features for each audio file
        label: label for each audio. Currently, they are 0(english), 1(german), 2(french), 3(spanish), 4(chinese)
        id: "" if not submit to SGE, otherwise it is the task id
        extend: dataset name, e.g "voxforge", "eu_repo", "youtube"
        
        id and extend are used to make sure imgs are not overwriting each other when submitting SGE
    """
    mkdir(img_dir)
    count = 0
    for _, mat in kaldi_io.read_mat_ark(cmvn_feat_ark):
#         assert np.squeeze(mat).shape == (-1,NUM_BINS)
        scipy.misc.imsave(img_dir+label+"_"+str(count)+"_"+id+"_"+extend+".png", np.squeeze(mat).T)
        count += 1
        
        
        
if __name__ == "__main__":
    
    special_key = "1"
    
    # Log hostname when submitting to the SGE
    if 'HOSTNAME' in os.environ:
        hostname = os.environ['HOSTNAME']
    else:
        hostname = 'unknown host'
    timestr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
    print('executing {} on {} at {} ...'.format(sys.argv[0], hostname, timestr))
        
    # Environment Set up
    os.environ['PATH'] = "/f/gfs1/yiyangli/kaldi/src/featbin:" + os.environ['PATH']
    print "start loading params"
    
    
    id = None
    if SUBMIT == "False":
        # path
        id = "0" # default value
#         wav_files = []
#         root_wav = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/"+LANG+"/"
#         for w in os.listdir(root_wav):
#             if w[-4:] == ".wav":
#                 wav_files.append(root_wav+w)
#          # data/english_0/xxx.wav
        
#         chunk_dir = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/chunk_"+LANG+"_"+id+"/"
#         mkdir("/f/gfs1/yiyangli/lang_detect/data/youtube_test/imgs/")
#         img_dir = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/imgs/"
#         mkdir(img_dir)
#         mfcc_scp = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/mfcc_scp_"+LANG+"_"+id+".scp"
#         mfcc_ark = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/mfcc_ark_"+LANG+"_"+id+".ark"
#         cmvn_scp = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/cmvn_scp_"+LANG+"_"+id+".scp"
#         cmvn_ark = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/cmvn_ark_"+LANG+"_"+id+".ark"
#         cmvn_feat_ark = "/f/gfs1/yiyangli/lang_detect/data/youtube_test/cmvn_feat_ark_"+LANG+"_"+id+".ark"
    else:
        # initilize id when submitting to SGE
        id = str((int(os.environ["SGE_TASK_ID"]) -1))
        
        wav_files = []
        if DIR[-1] != "/":
            DIR += "/" # youtube_train/
        ind = DIR.rfind("_")
        extend = DIR[:ind]
        
        # READ paths to wav files 
        with open("/f/gfs1/yiyangli/lang_detect/src/"+DIR+"/"+LANG+"_"+id+"_"+extend+".txt","r") as f:
            for row in f:
                row = row.strip()
                wav_files.append(row)
        
        # Define directories and file names
        img_dir = "/f/gfs1/yiyangli/language-id/imgs/"+"imgs_"+DIR+"/" # id+"_"+extend+
        mkdir(img_dir)
        chunk_dir = "/tmp/chunk_"+LANG+"_"+id+"_"+DIR+"/"
        mfcc_scp = "/tmp/mfcc_scp_"+LANG+DIR[:-1]+"_"+id+"_"+extend+".scp"
        mfcc_ark = "/tmp/mfcc_ark_"+LANG+DIR[:-1]+"_"+id+"_"+extend+".ark"
        cmvn_scp = "/tmp/cmvn_scp_"+LANG+DIR[:-1]+"_"+id+"_"+extend+".scp"
        cmvn_ark = "/tmp/cmvn_ark_"+LANG+DIR[:-1]+"_"+id+"_"+extend+".ark"
        cmvn_feat_ark = "/tmp/cmvn_feat_ark_"+LANG+DIR[:-1]+"_"+id+"_"+extend+".ark"
    
    # -----------------------------------------------------------------------
    # Language
    # -----------------------------------------------------------------------
    # label
    if LANG == "english":
        label = "0"
    elif LANG == "german":
        label = "1"
    elif LANG == "french":
        label = "2"
    elif LANG == "spanish":
        label = "3"
    elif LANG == "chinese":
        label = "4"
    
    # pre-clean
    rmdir(chunk_dir)
    rmdir(mfcc_scp)
    rmdir(mfcc_ark)
    rmdir(cmvn_scp)
    rmdir(cmvn_ark)
    rmdir(cmvn_feat_ark)
    
    # main steps to convert .wav files to images
    get_chunks(wav_files, chunk_dir)
    convert(chunk_dir)
    split(chunk_dir)
    compute_mfcc_cmvn(chunk_dir, mfcc_scp, mfcc_ark, cmvn_scp, cmvn_ark, cmvn_feat_ark)
    get_img(img_dir, cmvn_feat_ark, label, id, extend)
        
    # post-clean
    rmdir(chunk_dir)
    rmdir(mfcc_scp)
    rmdir(mfcc_ark)
    rmdir(cmvn_scp)
    rmdir(cmvn_ark)
    rmdir(cmvn_feat_ark)