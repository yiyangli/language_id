from subprocess import check_call

if __name__ == "__main__":
#     args = parser.parse_args()
    root = "/f/gfs1/yiyangli/lang_detect/tensorflow/"
    
    data_dir = "/f/gfs1/yiyangli/lang_detect/imgs_1min_mfcc_cmvn/"
    model_dir = root+"vision/experiments/"
    lrs = ["1e-5"]
    models = []
    for lr in lrs:
        models.append(model_dir+"mfcc_cmvn_"+lr)
    
    for mod in models:
        cmd_py = "source /f/gfs1/yiyangli/.lang_env/bin/activate"
        cmd_py += ";export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python2.7/dist-packages"
        cmd_py += ";cd "+root+"vision/"

        cmd_py += ";python evaluate.py --model_dir {model_dir} --data_dir {data_dir} --restore_from {model_dir}/best_weights > results_mfcc_cmvn.txt".format(model_dir=mod,data_dir=data_dir)

#         cmd_py += ";python train.py --model_dir {model_dir} --data_dir {data_dir} --restore_from {model_dir}/last_weights".format(model_dir=mod,data_dir=data_dir)
        print "cmd_py: ", cmd_py

        cmd = "qsub -cwd -shell no -b yes -N eval_mfcc_cmvn_1e-5 -l gpu=1 -pe smp 1 bash -c \"{cmd}\"".format(cmd=cmd_py)
        check_call(cmd, shell=True)

        
        # data
        # model->lr
        # eval/ train
        # task name