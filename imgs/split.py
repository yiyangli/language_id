import os
from PIL import Image
# import collections

id = (int(os.environ["SGE_TASK_ID"]) -1)
# id = 0
root = "/f/gfs1/yiyangli/language-id/imgs/"

print "id: ", id

wid = 998 # ~ 10s
c = 0
f_w = open(root+"src/im_"+str(id)+".txt","w")
with open(root+"all.txt","r") as f:
    for row in f:
#         print "read: ", c
        if c >= id*100 and c < (id+1)*100:
            parent, img = row.strip().split()
            
            im = Image.open(root+parent+"/"+img)
#             print "size: ", im.size
            if im.size[0] >= wid:
                f_w.write(row.strip()+"\n")
#                 print "write"
        if c >= (id+1)*100:
            break
        c += 1
f_w.close()